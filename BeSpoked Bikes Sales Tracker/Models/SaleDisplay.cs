﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeSpoked_Bikes_Sales_Tracker.Models
{
    public class SaleDisplay
    {
        public int SalesPersonId { get; set; }
        public string ProductName { get; set; }
        public string SalesPersonFirstName { get; set; }
        public string SalesPersonLastName { get; set; }
        public string CustomerFirstName { get; set; }
        public string CustomerLastName { get; set; }
        public DateTime SalesDate { get; set; }
        public decimal Price { get; set; }
        public decimal Commission { get; set; }
        public string SalesDateJustDate
        {
            get
            {
                return SalesDate.ToShortDateString();
            }
            set
            {
                SalesDateJustDate = value;
            }
        }
        public string SalesPersonFullName
        {
            get
            {
                return String.Format("{0} {1}", SalesPersonFirstName, SalesPersonLastName);
            }
            set
            {
                SalesPersonFullName = value;
            }
        }
        public string CustomerFullName
        {
            get
            {
                return String.Format("{0} {1}", CustomerFirstName, CustomerLastName);
            }
            set
            {
                CustomerFullName = value;
            }
        }
    }
}
