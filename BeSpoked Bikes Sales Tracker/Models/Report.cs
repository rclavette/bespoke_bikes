﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeSpoked_Bikes_Sales_Tracker.Models
{
    public class Report
    {
        public int TotalSales { get; set; }
        public decimal TotalSalesDollars { get; set; }
        public decimal TotalCommission { get; set; }
    }
}
