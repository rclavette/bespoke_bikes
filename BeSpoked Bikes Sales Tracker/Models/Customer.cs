﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeSpoked_Bikes_Sales_Tracker.Models
{
    public class Customer
    {
        public int CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string StreetName { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string Phone { get; set; }
        public DateTime StartDate { get; set; }
        public string StartDateJustDate
        {
            get
            {
                return StartDate.ToShortDateString();
            }
            set
            {
                StartDateJustDate = value;
            }
        }
        public string FullName
        {
            get
            {
                return String.Format("{0} {1}", FirstName, LastName);
            }
            set
            {
                FullName = value;
            }
        }
        public string CompleteAddress
        {
            get
            {
                return String.Format("{0} {1},{2} {3}", StreetName, City, State, PostalCode);
            }
            set
            {
                CompleteAddress = value;
            }
        }
    }
}
