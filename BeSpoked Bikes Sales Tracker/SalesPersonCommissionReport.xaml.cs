﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BeSpoked_Bikes_Sales_Tracker.Models;

namespace BeSpoked_Bikes_Sales_Tracker
{
    /// <summary>
    /// Interaction logic for SalesPersonCommissionReport.xaml
    /// </summary>
    public partial class SalesPersonCommissionReport : Window
    {
        public List<SaleDisplay> Sales;
        public List<SaleDisplay> Q1Sales;
        public List<SaleDisplay> Q2Sales;
        public List<SaleDisplay> Q3Sales;
        public List<SaleDisplay> Q4Sales;
        public string Q1Start = "01/01/19";
        public string Q1End = "03/31/19";
        public string Q2Start = "04/01/19";
        public string Q2End = "06/30/19";
        public string Q3Start = "07/01/19";
        public string Q3End = "09/30/19";
        public string Q4Start = "10/01/19";
        public string Q4End = "12/31/19";
        Report Q1;
        Report Q2;
        Report Q3;
        Report Q4;
        Report YTD = new Report();


        public SalesPersonCommissionReport()
        {
            InitializeComponent();
        }
        public SalesPersonCommissionReport(List<SaleDisplay> sales)
        {
            InitializeComponent();
            Sales = sales;
            GenerateCommissionReport();
        }

        public void GenerateCommissionReport()
        {
            Q1Sales = Sales.FindAll(x => x.SalesDate >= DateTime.Parse(Q1Start) && x.SalesDate <= DateTime.Parse(Q1End));
            Q2Sales = Sales.FindAll(x => x.SalesDate >= DateTime.Parse(Q2Start) && x.SalesDate <= DateTime.Parse(Q2End));
            Q3Sales = Sales.FindAll(x => x.SalesDate >= DateTime.Parse(Q3Start) && x.SalesDate <= DateTime.Parse(Q3End));
            Q4Sales = Sales.FindAll(x => x.SalesDate >= DateTime.Parse(Q4Start) && x.SalesDate <= DateTime.Parse(Q4End));

            Q1 = GenerateReportModel(Q1Sales);
            Q2 = GenerateReportModel(Q2Sales);
            Q3 = GenerateReportModel(Q3Sales);
            Q4 = GenerateReportModel(Q4Sales);

            DisplayReportData();
        }

        public Report GenerateReportModel(List<SaleDisplay> salesToCalculate)
        {
            Report report = new Report();
            report.TotalSales = salesToCalculate.Count();
            foreach(var item in salesToCalculate)
            {
                report.TotalSalesDollars += item.Price;
                report.TotalCommission += (item.Price * (item.Commission / 100));
            }
            YTD.TotalCommission += report.TotalCommission;
            YTD.TotalSales += report.TotalSales;
            YTD.TotalSalesDollars += report.TotalSalesDollars;
            return report;
        }

        public void DisplayReportData()
        {
            if(Q1 != null)
            {
                Q1NumSalesLabel.Content = Q1.TotalSales;
                Q1TotalCommissionLabel.Content = "$" + Q1.TotalCommission;
                Q1TotalSalesLabel.Content = "$" + Q1.TotalSalesDollars;
            }
            if(Q2 != null)
            {
                Q2NumSalesLabel.Content = Q2.TotalSales;
                Q2TotalCommissionLabel.Content = "$" + Q2.TotalCommission;
                Q2TotalSalesLabel.Content = "$" + Q2.TotalSalesDollars;
            }
            if (Q3 != null)
            {
                Q3NumSalesLabel.Content = Q3.TotalSales;
                Q3TotalCommissionLabel.Content = "$" + Q3.TotalCommission;
                Q3TotalSalesLabel.Content = "$" + Q3.TotalSalesDollars;
            }
            if (Q4 != null)
            {
                Q4NumSalesLabel.Content = Q4.TotalSales;
                Q4TotalCommissionLabel.Content = "$" + Q4.TotalCommission;
                Q4TotalSalesLabel.Content = "$" + Q4.TotalSalesDollars;
            }
            if(YTD != null)
            {
                YTDNumSalesLabel.Content = YTD.TotalSales;
                YTDTotalCommissionLabel.Content = "$" + YTD.TotalCommission;
                YTDTotalSalesLabel.Content = "$" + YTD.TotalSalesDollars;
            }
        }
    }
}
