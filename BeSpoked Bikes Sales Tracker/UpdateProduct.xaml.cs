﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BeSpoked_Bikes_Sales_Tracker.Models;

namespace BeSpoked_Bikes_Sales_Tracker
{
    /// <summary>
    /// Interaction logic for UpdateProduct.xaml
    /// </summary>
    public partial class UpdateProduct : Window
    {

        DataAccess da;
        Product Product;

        public UpdateProduct()
        {
            InitializeComponent();
        }

        public UpdateProduct(Product product)
        {
            InitializeComponent();
            populateTextBoxes(product);
            Product = product;
            da = new DataAccess();
        }

        private void populateTextBoxes(Product product)
        {
            ProductNameTextBox.Text = product.Name;
            ProductManufacturerTextBox.Text = product.Manufacturer;
            ProductStyleTextBox.Text = product.Style;
            ProductPriceTextBox.Text = product.PurchasePrice.ToString();
            ProductQuantityTextBox.Text = product.Quantity.ToString();
            ProductCommissionRateTextBox.Text = product.CommissionRate.ToString();
            ProductSalePriceTextBox.Text = product.SalePrice.ToString();
        }

        private Product GetProductData()
        {
            return new Product
            {
                Name = ProductNameTextBox.Text,
                Manufacturer = ProductManufacturerTextBox.Text,
                Style = ProductStyleTextBox.Text,
                PurchasePrice = Convert.ToInt32(ProductPriceTextBox.Text),
                SalePrice = Convert.ToInt32(ProductSalePriceTextBox.Text),
                Quantity = Convert.ToInt32(ProductQuantityTextBox.Text),
                CommissionRate = Convert.ToInt32(ProductCommissionRateTextBox.Text),
                ProductId = Product.ProductId,
            };
        }

        private void ValidatePriceTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void ProductSaveButton_Click(object sender, RoutedEventArgs e)
        {
            TextBox[] textBoxes = { ProductNameTextBox, ProductManufacturerTextBox, ProductStyleTextBox, ProductPriceTextBox, ProductQuantityTextBox, ProductCommissionRateTextBox, ProductSalePriceTextBox };
            DatePicker[] datePickers = { };
            ComboBox[] comboBoxes = { };

            if (Validation.HandleValidation(textBoxes, datePickers, comboBoxes))
            {
                Product productToUpdate = GetProductData();
                if (da.UpdateProduct(productToUpdate))
                {
                    this.Close();
                }
                else
                {
                    MessageBox.Show("DB update failed.");
                }
            }
        }
    }
}
