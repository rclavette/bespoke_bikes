﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace BeSpoked_Bikes_Sales_Tracker
{
    public static class Validation
    {
        public static bool HandleValidation(TextBox[] textBoxes, DatePicker[] datePickers, ComboBox[] comboBoxes)
        {
            for (int i = 0; i < textBoxes.Count(); i++)
            {
                if (textBoxes[i].Text == string.Empty)
                {
                    MessageBox.Show("Please fill in the empty text boxes");
                    textBoxes[i].Focus();
                    return false;
                }

            }
            for (int i = 0; i < datePickers.Count(); i++)
            {
                if (datePickers[i].SelectedDate == null)
                {
                    MessageBox.Show("Please fill in the empty date pickers");
                    datePickers[i].Focus();
                    return false;
                }
            }
            for(int i = 0; i < comboBoxes.Count(); i++)
            {
                if(comboBoxes[i].SelectedIndex == -1)
                {
                    MessageBox.Show("Please make a selection from each combo box");
                    comboBoxes[i].Focus();
                    return false;
                }
            }

            return true;
        }
    }
}
