﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BeSpoked_Bikes_Sales_Tracker.Models;

namespace BeSpoked_Bikes_Sales_Tracker
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        DataAccess da = new DataAccess();
        List<SalesPerson> salesPeople = new List<SalesPerson>();
        List<Product> products = new List<Product>();
        List<Customer> customers = new List<Customer>();
        List<SaleDisplay> sales = new List<SaleDisplay>();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void SalesPersonModalClosed(object sender, EventArgs e)
        {
            SalesPersonListView.ItemsSource = null;
            SalesPersonListView.ItemsSource = salesPeople;
        }

        private void ProductModalClosed(object sender, EventArgs e)
        {
            ProductsListView.ItemsSource = null;
            ProductsListView.ItemsSource = products;
        }

        private void CreateSaleModalClosed(object sender, EventArgs e)
        {
            sales = da.GetAllSalesForDisplay();
            SalesListView.ItemsSource = null;
            SalesListView.ItemsSource = sales;
        }

        private void FilterSalesByDates()
        {
            List<SaleDisplay> filteredSales = new List<SaleDisplay>();
            if (StartDatePicker.SelectedDate != null && EndDatePicker.SelectedDate != null)
            {
                filteredSales = sales.FindAll(x => (x.SalesDate >= StartDatePicker.SelectedDate && x.SalesDate <= EndDatePicker.SelectedDate));
            }
            else if(StartDatePicker.SelectedDate != null && EndDatePicker.SelectedDate == null)
            {
                filteredSales = sales.FindAll(x => x.SalesDate >= StartDatePicker.SelectedDate);
            }
            else if(StartDatePicker.SelectedDate == null && EndDatePicker.SelectedDate != null)
            {
                filteredSales = sales.FindAll(x => x.SalesDate <= EndDatePicker.SelectedDate);
            }
            SalesListView.ItemsSource = null;
            SalesListView.ItemsSource = filteredSales;
        }

        //Event Handlers

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var currentItem = ((ListBoxItem)SalesPersonListView.ContainerFromElement((Button)sender));
            SalesPerson salesPersonToGenerateReport = (SalesPerson)currentItem.Content;
            var salesPersonsSales = sales.FindAll(x => x.SalesPersonId == salesPersonToGenerateReport.SalesPersonId);
            SalesPersonCommissionReport spcr = new SalesPersonCommissionReport(salesPersonsSales);
            spcr.ShowDialog();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            salesPeople = da.GetAllSalesPeople();
            SalesPersonListView.ItemsSource = salesPeople;
            products = da.GetAllProducts();
            ProductsListView.ItemsSource = products;
            customers = da.GetAllCustomers();
            CustomerListView.ItemsSource = customers;
            sales = da.GetAllSalesForDisplay();
            SalesListView.ItemsSource = sales;
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (TabControl.SelectedIndex == 0)
            {
                salesPeople = da.GetAllSalesPeople();
            }
            else if (TabControl.SelectedIndex == 1)
            {
                products = da.GetAllProducts();
            }
            else if (TabControl.SelectedIndex == 2)
            {
                customers = da.GetAllCustomers();
            }
            else if(TabControl.SelectedIndex == 3)
            {
                sales = da.GetAllSalesForDisplay();
            }
        }

        private void SalesPersonListView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            SalesPerson selectedSalesPerson = (SalesPerson)SalesPersonListView.SelectedItem;
            UpdateSalesPerson usp = new UpdateSalesPerson(selectedSalesPerson);
            usp.Closed += SalesPersonModalClosed;
            usp.ShowDialog();
        }

        private void ProductsListView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Product selectedProduct = (Product)ProductsListView.SelectedItem;
            UpdateProduct up = new UpdateProduct(selectedProduct);
            up.Closed += ProductModalClosed;
            up.ShowDialog();
        }

        private void CreateSaleButton_Click(object sender, RoutedEventArgs e)
        {
            CreateSale cs = new CreateSale(products, salesPeople, customers);
            cs.Closed += CreateSaleModalClosed;
            cs.ShowDialog();
        }

        private void StartDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            EndDatePicker.DisplayDateStart = StartDatePicker.SelectedDate;
            FilterSalesByDates();
        }

        private void EndDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            StartDatePicker.DisplayDateEnd = EndDatePicker.SelectedDate;
            FilterSalesByDates();
        }

        private void ClearFiltersButton_Click(object sender, RoutedEventArgs e)
        {
            StartDatePicker.SelectedDate = null;
            EndDatePicker.SelectedDate = null;
            SalesListView.ItemsSource = null;
            SalesListView.ItemsSource = sales;
        }
    }
}
