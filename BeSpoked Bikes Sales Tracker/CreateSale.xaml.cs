﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BeSpoked_Bikes_Sales_Tracker.Models;

namespace BeSpoked_Bikes_Sales_Tracker
{
    /// <summary>
    /// Interaction logic for CreateSale.xaml
    /// </summary>
    public partial class CreateSale : Window
    {

        public List<Product> Products;
        public List<SalesPerson> SalesPeople;
        public List<Customer> Customers;
        public Product SelectedProduct;
        public SalesPerson SelectedSalesPerson;
        public Customer SelectedCustomer;
        public DataAccess da;
           

        public CreateSale()
        {
            InitializeComponent();
        }

        public CreateSale(List<Product> products, List<SalesPerson> salesPeople, List<Customer> customers)
        {
            InitializeComponent();
            da = new DataAccess();
            Products = products;
            SalesPeople = salesPeople;
            Customers = customers;

            ProductComboBox.ItemsSource = products;
            SalesPersonComboBox.ItemsSource = salesPeople;
            CustomerComboBox.ItemsSource = customers;
        }

        private void ProductComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SelectedProduct = (Product)ProductComboBox.SelectedItem;
        }

        private void SalesPersonComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SelectedSalesPerson = (SalesPerson)SalesPersonComboBox.SelectedItem;
        }

        private void CustomerComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SelectedCustomer = (Customer)CustomerComboBox.SelectedItem;
        }

        private void SaveSaleButton_Click(object sender, RoutedEventArgs e)
        {
            TextBox[] textBoxes = { };
            DatePicker[] datePickers = { SalesDatePicker };
            ComboBox[] comboBoxes = { CustomerComboBox, ProductComboBox, SalesPersonComboBox };

            if (Validation.HandleValidation(textBoxes, datePickers, comboBoxes))
            {
                Sale createdSale = new Sale
                {
                    ProductId = SelectedProduct.ProductId,
                    CustomerId = SelectedCustomer.CustomerId,
                    SalesPersonId = SelectedSalesPerson.SalesPersonId,
                    SalesDate = (DateTime)SalesDatePicker.SelectedDate,
                };

                if (da.CreateSale(createdSale))
                {
                    this.Close();
                }
                else
                {
                    MessageBox.Show("DB create failed");
                }
            }
        }
    }
}
