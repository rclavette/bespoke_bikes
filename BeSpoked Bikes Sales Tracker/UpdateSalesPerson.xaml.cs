﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BeSpoked_Bikes_Sales_Tracker.Models;

namespace BeSpoked_Bikes_Sales_Tracker
{
    /// <summary>
    /// Interaction logic for UpdateSalesPerson.xaml
    /// </summary>
    public partial class UpdateSalesPerson : Window
    {
        DataAccess da;
        SalesPerson SalesPerson;

        public UpdateSalesPerson()
        {
            InitializeComponent();
        }

        public UpdateSalesPerson(SalesPerson salesPerson)
        {
            InitializeComponent();
            populateTextBoxes(salesPerson);
            SalesPerson = salesPerson;
            da = new DataAccess();
        }

        private void populateTextBoxes(SalesPerson salesPerson)
        {
            CityTextBox.Text = salesPerson.City;
            FirstNameTextBox.Text = salesPerson.FirstName;
            LastNameTextBox.Text = salesPerson.LastName;
            StreetTextBox.Text = salesPerson.StreetName;
            StateTextBox.Text = salesPerson.State;
            PostalCodeTextBox.Text = salesPerson.PostalCode;
            PhoneTextBox.Text = salesPerson.Phone;
            StartDatePicker.SelectedDate = salesPerson.StartDate;
            TerminationDatePicker.SelectedDate = (salesPerson.TerminationDate == null) ? DateTime.MinValue : (DateTime)salesPerson.TerminationDate;
            ManagerTextBox.Text = salesPerson.Manager;
        }


        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            TextBox[] textBoxes = { FirstNameTextBox, LastNameTextBox, StreetTextBox, CityTextBox, StateTextBox, PostalCodeTextBox, PhoneTextBox, ManagerTextBox };
            DatePicker[] datePickers = { StartDatePicker, TerminationDatePicker };
            ComboBox[] comboBoxes = { };

            if (Validation.HandleValidation(textBoxes, datePickers, comboBoxes))
            {
                SalesPerson salesPersonToUpdate = GetSalesPersonData();
                if (da.UpdateSalesPerson(salesPersonToUpdate))
                {
                    this.Close();
                }
                else
                {
                    MessageBox.Show("DB update failed.");
                }
            }
        }

        private SalesPerson GetSalesPersonData()
        {
            return new SalesPerson
            {
                FirstName = FirstNameTextBox.Text,
                LastName = LastNameTextBox.Text,
                StreetName = StreetTextBox.Text,
                City = CityTextBox.Text,
                State = StateTextBox.Text,
                PostalCode = PostalCodeTextBox.Text,
                Phone = PhoneTextBox.Text,
                StartDate = (DateTime)StartDatePicker.SelectedDate,
                TerminationDate = TerminationDatePicker.SelectedDate,
                Manager = ManagerTextBox.Text,
                SalesPersonId = SalesPerson.SalesPersonId,
            };
        }
    }
}
