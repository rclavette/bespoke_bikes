﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BeSpoked_Bikes_Sales_Tracker.Models;

namespace BeSpoked_Bikes_Sales_Tracker
{
    public class DataAccess
    {
        string dbConn = "";

        public List<SalesPerson> GetAllSalesPeople()
        {
            using(SqlConnection conn = new SqlConnection(dbConn))
            {
                List<SalesPerson> salesPeople = new List<SalesPerson>();
                string query = "SELECT * FROM SalesPeople";
                SqlCommand cmd = CreateSqlCommand(query, conn);
                conn.Open();
                try
                {
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            salesPeople.Add(new SalesPerson
                            {
                                SalesPersonId = Convert.ToInt32(reader["SalesPersonId"]),
                                FirstName = reader["FirstName"].ToString(),
                                LastName = reader["LastName"].ToString(),
                                StreetName = reader["StreetName"].ToString(),
                                City = reader["City"].ToString(),
                                State = reader["State"].ToString(),
                                PostalCode = reader["PostalCode"].ToString(),
                                Phone = reader["Phone"].ToString(),
                                StartDate = Convert.ToDateTime(reader["StartDate"]),
                                TerminationDate = reader["TerminationDate"] as DateTime?,
                                Manager = reader["Manager"].ToString(),
                        });
                        }
                    }
                    conn.Close();
                } catch(Exception ex)
                {
                }
                return salesPeople;
            }
        }

        public List<Sale> GetAllSales()
        {
            using(SqlConnection conn = new SqlConnection(dbConn))
            {
                List<Sale> sales = new List<Sale>();
                string query = "SELECT * FROM Sales";
                SqlCommand cmd = CreateSqlCommand(query, conn);
                conn.Open();
                try
                {
                    using(SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            sales.Add(new Sale
                            {
                                ProductId = Convert.ToInt32(reader["ProductId"]),
                                SalesPersonId = Convert.ToInt32(reader["SalesPersonId"]),
                                CustomerId = Convert.ToInt32(reader["CustomerId"]),
                                SalesDate = Convert.ToDateTime(reader["SalesDate"])
                            });
                        }
                    } 
                }
                catch (Exception ex) { }
                return sales;
            }
        }

        public List<Product> GetAllProducts()
        {
            using (SqlConnection conn = new SqlConnection(dbConn))
            {
                List<Product> products = new List<Product>();
                string query = "SELECT * FROM Products";
                SqlCommand cmd = CreateSqlCommand(query, conn);
                conn.Open();
                try
                {
                    using(SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            products.Add(new Product
                            {
                                ProductId = Convert.ToInt32(reader["ProductId"]),
                                Name = reader["Name"].ToString(),
                                Manufacturer = reader["Manufacturer"].ToString(),
                                Style = reader["Style"].ToString(),
                                PurchasePrice = Convert.ToDecimal(reader["PurchasePrice"]),
                                SalePrice = Convert.ToDecimal(reader["SalePrice"]),
                                Quantity = Convert.ToInt32(reader["Quantity"]),
                                CommissionRate = Convert.ToInt32(reader["CommissionRate"]),
                            });
                        }
                    }
                    conn.Close();
                } catch(Exception ex)
                {
                }
                return products;
            }
        }

        public List<Customer> GetAllCustomers()
        {
            using (SqlConnection conn = new SqlConnection(dbConn))
            {
                List<Customer> customers = new List<Customer>();
                string query = "SELECT * FROM Customer";
                SqlCommand cmd = CreateSqlCommand(query, conn);
                conn.Open();
                try
                {
                    using(SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            customers.Add(new Customer
                            {
                                CustomerId = Convert.ToInt32(reader["CustomerId"]),
                                FirstName = reader["FirstName"].ToString(),
                                LastName = reader["LastName"].ToString(),
                                StreetName = reader["StreetName"].ToString(),
                                City = reader["City"].ToString(),
                                State = reader["State"].ToString(),
                                PostalCode = reader["PostalCode"].ToString(),
                                Phone = reader["Phone"].ToString(),
                                StartDate = Convert.ToDateTime(reader["StartDate"])
                            });
                        }
                    }
                    conn.Close();
                } catch(Exception ex) { }
                return customers;
            }
        }

        public List<SaleDisplay> GetAllSalesForDisplay()
        {
            using (SqlConnection conn = new SqlConnection(dbConn))
            {
                List<SaleDisplay> salesToDisplay = new List<SaleDisplay>();
                SqlCommand cmd = CreateSqlCommand("dbo.GetAllSalesForDisplay", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                conn.Open();
                try
                {
                    using(SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            salesToDisplay.Add(new SaleDisplay
                            {
                                SalesPersonId = Convert.ToInt32(reader["SalesPersonId"]),
                                ProductName = reader["ProdName"].ToString(),
                                SalesPersonFirstName = reader["SpFirstName"].ToString(),
                                SalesPersonLastName = reader["SpLastName"].ToString(),
                                CustomerFirstName = reader["CFirstName"].ToString(),
                                CustomerLastName = reader["CLastName"].ToString(),
                                SalesDate = Convert.ToDateTime(reader["salesdate"]),
                                Price = Convert.ToDecimal(reader["SalePrice"]),
                                Commission = Convert.ToInt32(reader["CommissionRate"]),
                            });
                        }
                    }
                    conn.Close();
                }
                catch(Exception ex) { }
                return salesToDisplay;
            }
        }

        public bool UpdateProduct(Product updatedProduct)
        {
            using(SqlConnection conn = new SqlConnection(dbConn))
            {
                SqlCommand cmd = CreateSqlCommand("dbo.UpdateProduct", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@name", updatedProduct.Name);
                cmd.Parameters.AddWithValue("@manufacturer", updatedProduct.Manufacturer);
                cmd.Parameters.AddWithValue("@style", updatedProduct.Style);
                cmd.Parameters.AddWithValue("@purchaseprice", updatedProduct.PurchasePrice);
                cmd.Parameters.AddWithValue("@quantity", updatedProduct.Quantity);
                cmd.Parameters.AddWithValue("@commissionRate", updatedProduct.CommissionRate);
                cmd.Parameters.AddWithValue("@productId", updatedProduct.ProductId);
                cmd.Parameters.AddWithValue("@saleprice", updatedProduct.SalePrice);

                conn.Open();
                try
                {
                    cmd.ExecuteReader();
                    return true;
                }
                catch(Exception ex)
                {
                    return false;
                }
            }
        }

        public bool UpdateSalesPerson(SalesPerson updatedSalesPerson)
        {
            using (SqlConnection conn = new SqlConnection(dbConn))
            {
                SqlCommand cmd = CreateSqlCommand("dbo.UpdateSalesPerson", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@firstName", updatedSalesPerson.FirstName);
                cmd.Parameters.AddWithValue("@lastName", updatedSalesPerson.LastName);
                cmd.Parameters.AddWithValue("@streetName", updatedSalesPerson.StreetName);
                cmd.Parameters.AddWithValue("@city", updatedSalesPerson.City);
                cmd.Parameters.AddWithValue("@state", updatedSalesPerson.State);
                cmd.Parameters.AddWithValue("@postalCode", updatedSalesPerson.PostalCode);
                cmd.Parameters.AddWithValue("@phone", updatedSalesPerson.Phone);
                cmd.Parameters.AddWithValue("@startDate", updatedSalesPerson.StartDate);
                if (updatedSalesPerson.TerminationDate.Value.Year > 1753)
                {
                    cmd.Parameters.AddWithValue("@terminationDate", updatedSalesPerson.TerminationDate);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@terminationDate", DBNull.Value);
                }
                cmd.Parameters.AddWithValue("@manager", updatedSalesPerson.Manager);
                cmd.Parameters.AddWithValue("@salesPersonId", updatedSalesPerson.SalesPersonId);

                conn.Open();
                try
                {
                    var test = cmd.ExecuteReader();
                    return true;
                }
                catch ( Exception ex )
                { return false; }
            }
        }

        public bool CreateSale(Sale createdSale)
        {
            using (SqlConnection conn = new SqlConnection(dbConn))
            {
                SqlCommand cmd = CreateSqlCommand("dbo.CreateSale", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@productId", createdSale.ProductId);
                cmd.Parameters.AddWithValue("@salesPersonId", createdSale.SalesPersonId);
                cmd.Parameters.AddWithValue("@customerId", createdSale.CustomerId);
                cmd.Parameters.AddWithValue("@salesDate", createdSale.SalesDate);

                conn.Open();
                try
                {
                    cmd.ExecuteReader();
                    return true;
                } catch(Exception ex)
                {
                    return false;
                }
            }
        }

        private SqlCommand CreateSqlCommand(string query, SqlConnection conn)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = query;
            cmd.Connection = conn;
            return cmd;
        }
    }
}
